/*jslint
  browser,
  devel,
  for,
  this,
  single,
  white,
  long
*/
/*global
  window
*/

/**
 * @constructor
 */
var MeoBox2 = function( argConfig ) {
  
  "use strict";
  
  this.version  = "2.0.0";
  
  this.config   = this.parseConfig( argConfig );

  this.init( this.config );
  
};

MeoBox2.prototype.parseConfig = function( argConfig ) {
  
  "use strict";
  
  var config          = {};
  
  config.class        = ( "undefined" !== typeof argConfig.class       ? argConfig.class.toString().trim()      : "" );

  config.id           = ( "undefined" !== typeof argConfig.id           ? argConfig.id.toString().trim()          : "" );

  config.width        = ( "undefined" !== typeof argConfig.width        ? parseInt( argConfig.width, 10 )         : 500 );

  config.top          = ( "undefined" !== typeof argConfig.top          ? parseInt( argConfig.top, 10 )           : false );

  config.left         = ( "undefined" !== typeof argConfig.left         ? parseInt( argConfig.left, 10 )          : false );

  config.marginLeft   = ( "undefined" !== typeof argConfig.marginLeft   ? parseInt( argConfig.marginLeft, 10 )    : false );

  config.marginRight  = ( "undefined" !== typeof argConfig.marginRight  ? parseInt( argConfig.marginRight, 10 )   : false );
  
  config.zIndex       = ( "undefined" !== typeof argConfig.zIndex       ? parseInt( argConfig.zIndex, 10 )        : 100 );
  
  config.title        = ( "undefined" !== typeof argConfig.title        ? argConfig.title.toString().trim()       : "MeoBox2" );
  
  config.bodyHTML     = ( "undefined" !== typeof argConfig.bodyHTML     ? argConfig.bodyHTML.toString().trim()    : "" );

  config.appendBody   = ( "undefined" !== typeof argConfig.appendBody   ? argConfig.appendBody                    : null );

  config.footerHTML   = ( "undefined" !== typeof argConfig.footerHTML   ? argConfig.footerHTML.toString().trim()  : "" );

  config.appendFooter = ( "undefined" !== typeof argConfig.appendFooter ? argConfig.appendFooter                  : null );
  
  config.appendTo     = ( "undefined" !== typeof argConfig.appendTo     ? argConfig.appendTo                      : document.body );
  
  config.buttons      = {
    cancel    : {
      display   : false,
      label     : "Cancel",
      title     : "Cancel",
      callback  : function() {}
    },
    abort     : {
      display   : false,
      label     : "Abort",
      title     : "Abort",
      callback  : function() {}
    },
    okay      : {
      display   : false,
      label     : "Okay",
      title     : "Okay",
      callback  : function() {}
    },
    minimize  : {
      display   : true,
      label     : "&#95;",
      title     : "Minimize",
      callback  : function() {}
    },
    maximize  : {
      display   : true,
      label     : "&square;",
      title     : "Maximize",
      callback  : function() {}
    },
    close     : {
      display   : true,
      label     : "&#x2716;",
      title     : "Close",
      callback  : function() {}
    }
  };

  Object.keys( config.buttons ).forEach( function( button ) {
    
    if( "undefined" === typeof argConfig.buttons ) {
      return;
    }

    if( "undefined" === typeof argConfig.buttons[ button ] ) {
      return;
    }

    if( argConfig.buttons[ button ] === false ) {
      config.buttons[ button ].display = false;
    }
    else {

      if( "undefined" !== typeof argConfig.buttons[ button ].display ) {
        config.buttons[ button ].display = argConfig.buttons[ button ].display;
      }

      if( "undefined" !== typeof argConfig.buttons[ button ].label ) {
        config.buttons[ button ].label = argConfig.buttons[ button ].label.toString().trim();
      }

      if( "undefined" !== typeof argConfig.buttons[ button ].title ) {
        config.buttons[ button ].title = argConfig.buttons[ button ].title.toString().trim();
      }
    
      if( "undefined" !== typeof argConfig.buttons[ button ].callback && typeof argConfig.buttons[ button ].callback === "function" ) {
        config.buttons[ button ].callback = argConfig.buttons[ button ].callback;
      }

    }

  });

  return config;
  
};

MeoBox2.prototype.createDOM = function( argConfig ) {
  
  "use strict";
  
  var config    = this.parseConfig( argConfig );
  var fragment  = document.createDocumentFragment();
  var el        = {};

  /* Container */
  el.meoBox2 = document.createElement("div");
  el.meoBox2.setAttribute( "class", "meoBox2" + ( config.class !== "" ? " " + config.class : "" ) );
  
  if( config.id !== "" ) {
    el.meoBox2.setAttribute( "id", config.id );
  }
  
  if( config.top !== false ) {
    el.meoBox2.style.top = config.top + "px";
  }
  
  if( config.left !== false ) {
    el.meoBox2.style.left= config.left + "px";
  }

  if( config.marginLeft !== false ) {
    el.meoBox2.style.marginLeft = config.marginLeft + "px";
  }

  if( config.marginRight !== false ) {
    el.meoBox2.style.marginRight = config.marginRight + "px";
  }

  if( config.width !== false ) {
    el.meoBox2.style.width  = config.width + "px";
  }
  
  /* Header */
  el.header = document.createElement("div");
  el.header.setAttribute( "class", "meoBox2-header meoBox2-group" );
  
  /* Header: Title */
  el.headerTitle = document.createElement("div");
  el.headerTitle.setAttribute( "class", "meoBox2-header-title" );
  el.headerTitle.innerHTML = config.title;
  
  el.header.appendChild( el.headerTitle );
  
  /* Header: UI: BtnClose */
  if( config.buttons.close.display !== false ) {

    el.headerBtnClose = document.createElement("div");
    el.headerBtnClose.setAttribute( "class", "meoBox2-header-btn meoBox2-header-btnClose" );
    el.headerBtnClose.setAttribute( "title", "Close" );
    el.headerBtnClose.innerHTML = "&#x2716;";
    el.headerBtnClose.addEventListener( "click", function() {
      if( "function" === typeof config.buttons.close.callback ) {
        config.buttons.close.callback();
      }
      this.parentElement.parentElement.remove();
      return false;
    });
    
    el.header.appendChild( el.headerBtnClose );

  }
  
  /* Header: UI: BtnMaximize */
  if( config.buttons.maximize.display !== false ) {

    el.headerBtnMaximize = document.createElement("div");
    el.headerBtnMaximize.setAttribute( "class", "meoBox2-header-btn meoBox2-header-btnMaximize" );
    el.headerBtnMaximize.setAttribute( "title", "Maximize" );
    el.headerBtnMaximize.innerHTML = "&square;";
    el.headerBtnMaximize.addEventListener( "click", function() {
      
      var parent = this.parentElement;
      
      parent.classList.remove("meoBox2-hideSiblings");
      parent.parentElement.classList.remove("meoBox2-minimized");

      return false;

    });
    
    el.header.appendChild( el.headerBtnMaximize );
 
  }

  
  /* Header: UI: BtnMinimize */
  if( config.buttons.minimize.display !== false ) {

    el.headerBtnMinimize = document.createElement("div");
    el.headerBtnMinimize.setAttribute( "class", "meoBox2-header-btn meoBox2-header-btnMinimize" );
    el.headerBtnMinimize.setAttribute( "title", "Minimize" );
    el.headerBtnMinimize.innerHTML = "&#95;";
    el.headerBtnMinimize.addEventListener( "click", function() {
      
      var parent = this.parentElement;
      
      parent.classList.add("meoBox2-hideSiblings");
      parent.parentElement.classList.add("meoBox2-minimized");

      return false;
    });
  
    el.header.appendChild( el.headerBtnMinimize );

  }

  /* Body */
  el.body = document.createElement("div");
  el.body.setAttribute( "class", "meoBox2-body" );
  el.body.innerHTML = config.bodyHTML;

  /* Body: Custom DOM elements */
  if( config.appendBody !== null ) {
    
    el.body.appendChild( config.appendBody );
    
  }

  
  /* Footer */
  el.footer = document.createElement("div");
  el.footer.setAttribute( "class", "meoBox2-footer meoBox2-group" );
  
  /* Footer: BtnCancel */
  if( config.buttons.cancel.display !== false ) {
    
    el.footerBtnCancel = document.createElement("div");
    el.footerBtnCancel.setAttribute( "class", "meoBox2-footer-btn meoBox2-footer-btnCancel" );
    el.footerBtnCancel.setAttribute( "title", config.buttons.cancel.title );
    el.footerBtnCancel.innerHTML = config.buttons.cancel.label;
    el.footerBtnCancel.addEventListener( "click", config.buttons.cancel.callback );
    
    el.footer.appendChild( el.footerBtnCancel );
    
  }

  /* Footer: BtnAbort */
  if( config.buttons.abort.display !== false ) {
    
    el.footerBtnAbort = document.createElement("div");
    el.footerBtnAbort.setAttribute( "class", "meoBox2-footer-btn meoBox2-footer-btnAbort" );
    el.footerBtnAbort.setAttribute( "title", config.buttons.abort.title );
    el.footerBtnAbort.innerHTML = config.buttons.abort.label;
    el.footerBtnAbort.addEventListener( "click", config.buttons.abort.callback );
    
    el.footer.appendChild( el.footerBtnAbort );
    
  }

  /* Footer: BtnOkay */
  if( config.buttons.okay.display !== false ) {
    
    el.footerBtnOkay = document.createElement("div");
    el.footerBtnOkay.setAttribute( "class", "meoBox2-footer-btn meoBox2-footer-btnOkay" );
    el.footerBtnOkay.setAttribute( "title", config.buttons.okay.title );
    el.footerBtnOkay.innerHTML = config.buttons.okay.label;
    el.footerBtnOkay.addEventListener( "click", config.buttons.okay.callback );
    
    el.footer.appendChild( el.footerBtnOkay );
    
  }

  /* Footer: Custom DOM elements */
  if( config.appendFooter !==  null ) {

    el.footer.appendChild( config.appendFooter );
    
  }
  
  /* Assemble MeoBox2 */
  el.meoBox2.appendChild( el.header );
  el.meoBox2.appendChild( el.body );

  el.meoBox2.appendChild( el.footer );

  
  fragment.appendChild( el.meoBox2 );
  
  return fragment;
  
};

MeoBox2.prototype.init  = function( argConfig ) {

  "use strict";
  
  var config  = this.parseConfig( argConfig );
  var box     = this.createDOM( config );

  config.appendTo.appendChild( box );

};
