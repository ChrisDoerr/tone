
import { prepStringValue, prepNumericValue }            from "./Helpers.js";
import { Subscription, createSubscription }             from "./Data/Subscription.js";
import { Category, createCategory }                     from "./Data/Category.js";
import { Subscriptions }                                from "./Data/Subscriptions.js";
import { SubscriptionResult, createSubscriptionResult } from "./Data/SubscriptionResult.js";

export default {

  /**
   * Helper: Is "Subscriptions" object
   */
  isSubscriptions( stack ) {

    return (
      "object" === typeof stack &&
      ( stack instanceof Subscriptions || stack.constructor.name === "Subscriptions" )
    );
  
  },
  
  /**
   * Helper: Is "Category" object
   */
  isCategory( category ) {

    return (
      "object" === typeof category &&
      ( category instanceof Category || category.constructor.name === "Category" )
    );
  
  },

  /**
   * Helper: Is "Subscription" object
   */
  isSubscription( subscription ) {

    return (
      "object" === typeof subscription &&
      ( subscription instanceof Subscription || subscription.constructor.name === "Subscription" )
    );
  
  },
  
  /**
   * Helper: Id Exists
   */
  idExists( stack, id ) {

    if( !this.isSubscriptions( stack ) ) {
      return false;
    }
    
    let catIndex    = 0;
    let dimCat      = stack.data.length;
  
    for( catIndex   = 0; catIndex < dimCat; catIndex += 1 ) {
    
      if( stack.data[ catIndex ].id === id ) {
        return true;
      }
    
      let subIndex  = 0;
      let dimSub    = stack.data[ catIndex ].subscriptions.length;
    
      for( subIndex = 0; subIndex < dimSub; subIndex += 1 ) {
      
        if( stack.data[ catIndex ].subscriptions[ subIndex ].id === id ) {
          return true;
        }
      
      }
    
    }
  
    return false;
  
  },

  /**
   * Helper: Generate (unique) ID
   * Unique within a given "Subscptions" stack object.
   *
   * @returns INT or FALSE if stack is not a valid "Subscriptions" object
   */
  generateId( stack ) {

    if( !this.isSubscriptions( stack ) ) {
      return false;
    }
  
    let id = 0;
  
    do {
    
      id += 1;
    
    }
    while( this.idExists( stack, id ) );
  
    return id;

  },

  /**
   * Helper: Get Category Index
   * @returns If the category was foud, an integer (=stack index) is returned, otherwise FALSE. Also FALSE, if the stack is not an object of the "Subscriptions" class.
   */
  getCategoryIndex( stack, byProperty, byValue, prepValue ) {

    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    let prop          = byProperty.trim();
    let value         = ( "function" === typeof prepValue ? prepValue( byValue ) : byValue );

    let index         = false;

    let dimCategories = stack.data.length;
    let i             = 0;

    for( i = 0; i < dimCategories; i += 1 ) {

      let val         = ( "undefined" === typeof stack.data[i][ prop ] ? null : stack.data[i][ prop ] );
      
      if( null === val ) {
        console.log( `Property [${prop}] does not exits!` );
        continue;
      }

      if( value === ( "function" === typeof prepValue ? prepValue( val ) : val ) ) {
      
        index = i;
        break;
      
      }
    
    }
  
    return index;

  },
  
  /**
   * Get Category Index By Name
   * Wrapper, calling prepped getCategoryIndex()
   *
   * @returns INT (stack index) or FALSE if not found
   */
  getCategoryIndexByName( stack, name ) {

    return this.getCategoryIndex( stack, "name", name, prepStringValue );
  
  },

  /**
   * Get Category Index By ID
   * Wrapper, calling prepped getCategoryIndex()
   *
   * @returns INT (stack index) or FALSE if not found
   */
  getCategoryIndexById( stack, id ) {
  
    return this.getCategoryIndex( stack, "id", id, prepNumericValue );
  
  },

  /**
   * Get Category By Name
   *
   * @returns "Category" object or NULL if not found
   */
  getCategoryByName( stack, name ) {
  
    let catIndex = this.getCategoryIndexByName( stack, name );

    if(
      "number" === typeof catIndex &&
      this.isCategory( stack.data[ catIndex ] )
    ) {
      
      return stack.data[ catIndex ];
      
    }
    else {
      
      return null;
      
    }
  
  },

  /**
   * Get Category By Id
   *
   * @returns "Category" object or NULL if not found
   */
  getCategoryById( stack, id ) {
  
    let catIndex = this.getCategoryIndexById( stack, id );
  
    if(
      "number" === typeof catIndex &&
      this.isCategory( stack.data[ catIndex ] )
    ) {
      
      return stack.data[ catIndex ];
      
    }
    else {
      
      return null;
      
    }

  },

  /**
   * Get Category By Index
   *
   * @returns "Category" object or NULL if not found
   */
  getCategoryByIndex( stack, index ) {
    
    if(
      !this.isSubscriptions( stack ) ||
      !this.isCategory( stack.data[ index ] )
    ) {

      return null;

    }
    else {

      return stack.data[ index ];

    }

  },
  
  /**
   * Category Exists
   * ...by matching .id or .name or .title
   *
   * @returns BOOLEAN
   */
  categoryExists( stack, category ) {

    if(
      !this.isSubscriptions( stack ) ||
      !this.isCategory( category )
    ) {
      return false;
    }

    let catName       = category.name.trim().toLowerCase();
    let catIndex      = 0;
    let dimCategories = stack.data.length;

    for( catIndex = 0; catIndex < dimCategories; catIndex += 1 ) {
    
      if(
        ( stack.data[ catIndex ].name.trim().toLowerCase() === catName ) ||
        ( stack.data[ catIndex ].id === category.id )
      ) {

        return true;
      
      }
    
    }
  
    return false;
  
  },
  
  getSubscription( stack, byProperty, byValue, prepValue ) {

    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    let property    = byProperty.trim().toLowerCase();

    // @todo do the property detection only once!
    const validProperties = ( Object.getOwnPropertyNames( new Subscription({}) ) ).map( val => val.toLowerCase() );

    if( !validProperties.includes( property ) ) {
      return false;
    }

    let value       = ( "function" === typeof prepValue ? prepValue( byValue ) : byValue );

    let catIndex    = 0;
    let dimCats     = stack.data.length;

    for( catIndex = 0; catIndex < dimCats; catIndex += 1 ) {

      let subIndex  = 0;
      let dimSubs   = stack.data[ catIndex ].subscriptions.length;

      for( subIndex = 0; subIndex < dimSubs; subIndex += 1 ) {

        let subVal = stack.data[ catIndex ].subscriptions[ subIndex ][ byProperty ];

        if( "function" === typeof prepValue ) {
          subVal = prepValue( subVal );
        }

        if( subVal === value ) {

          return createSubscriptionResult({
            categoryIndex     : catIndex,
            subscriptionIndex : subIndex,
            subscription      : stack.data[ catIndex ].subscriptions[ subIndex ]
          });
          
        }

      }

    }

    return false;

  },

  /**
   * Wrapper: Get Subscription by name
   */
  getSubscriptionByName( stack, subscriptionName ) {

    return this.getSubscription( stack, "name", subscriptionName, prepStringValue );

    /*
    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    let subName   = subscriptionName.trim().toLowerCase();

    let catIndex  = 0;
    let dimCats   = stack.data.length;

    for( catIndex = 0; catIndex < dimCats; catIndex += 1 ) {

      let subIndex  = 0;
      let dimSubs   = stack.data[ catIndex ].subscriptions.length;

      for( subIndex = 0; subIndex < dimSubs; subIndex += 1 ) {

        if( stack.data[ catIndex ].subscriptions[ subIndex ].name.trim().toLowerCase() === subName ) {

          return createSubscriptionResult({
            categoryIndex     : catIndex,
            subscriptionIndex : subIndex,
            subscription      : stack.data[ catIndex ].subscriptions[ subIndex ]
          });

        }

      }

    }
    */

  },

  /**
   * Wrapper: Get Subscription By Id
   *
   * @returns "SubscriptionResult" object or FALSE if not found.
   */
  getSubscriptionById( stack, subscriptionId ) {

    return this.getSubscription( stack, "id", subscriptionId, prepNumericValue );

    /*
    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    let catIndex    = 0;
    let dimCats     = stack.data.length;
  
    for( catIndex = 0; catIndex < dimCats; catIndex += 1 ) {
    
      let subIndex  = 0;
      let dimSubs   = stack.data[ catIndex ].subscriptions.length;
    
      for( subIndex = 0; subIndex < dimSubs; subIndex += 1 ) {
      
        if( stack.data[ catIndex ].subscriptions[ subIndex ].id === subscriptionId ) {
          
          return createSubscriptionResult({
            categoryIndex     : catIndex,
            subscriptionIndex : subIndex,
            subscription      : stack.data[ catIndex ].subscriptions[ subIndex ]
          });

        }
      
      }
    
    }
  
    return false;
    */
  
  },

  closeAllCategories( stack ) {

    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    let catIndex  = 0;
    let dimCats   = stack.data.length;

    for( catIndex = 0; catIndex < dimCats; catIndex += 1 ) {

      stack.data[ catIndex ].visibility = "closed";

    }

    return stack;

  },

  /**
   * Subscription Exists
   * ...by matching .id or .name
   *
   * @returns "Subscription" object or FALSE
   */
  subscriptionExists( stack, subscription ) {
    
    if(
      !this.isSubscriptions( stack ) ||
      !this.isSubscription( subscription )
    ) {

      return false;

    }


    let catIndex      = 0;
    let dimCategories = stack.data.length;
  
    for( catIndex = 0; catIndex < dimCategories; catIndex += 1 ) {
    
      let subIndex    = 0;
      let dimSubs     = stack.data[ catIndex ].subscriptions.length;
    
      for( subIndex = 0; subIndex < dimSubs; subIndex += 1 ) {
      
        let sub       = stack.data[ catIndex ].subscriptions[ subIndex ];
      
        if(
          ( sub.name.trim().toLowerCase() === subscription.name.trim().toLowerCase() ) ||
          ( sub.id === subscription.id ) ||
          ( sub.title.trim().toLowerCase() === subscription.title.trim().toLowerCase() )
        ) {

          return sub;

        }
      
      }
    
    }

    return false;

  },
  
  /**
   * Add New Category
   *
   * @returns or FALSE
   */
  addNewCategory( stack, category ) {

    if(
      !this.isSubscriptions( stack ) ||
      !this.isCategory( category ) ||
      this.categoryExists( stack, category )
    ) {

      return false;

    }

    stack.data.push( category );

    return true;

  },


  /**
   * Add New Subscription
   *
   * @returns "Subscription" object (where, for example, the ID might have been generated) or FALSE
   */
  addNewSubscription( stack, subscription, categoryId ) {
   
    if(
      !this.isSubscriptions( stack ) ||
      !this.isSubscription( subscription )
    ) {

      return false;

    }

  
    // If not set, generate a unique (enough) ID
    if( null === subscription.id || subscription.id < 0 ) {

      subscription.id = this.generateId();

    }

    let sub = this.subscriptionExists( stack, subscription );
  
    if( true === sub ) {
      return false;
    }
  
    let catIndex = this.getCategoryIndexById( stack, categoryId );
  
    if( null === catIndex ) {
      return false;
    }
  
    stack.data[ catIndex ].subscriptions.push( subscription );

    return subscription;
  
  },

  /**
   * Update Category
   *
   * @returns BOOLEAN
   */
  updateCategory( stack, category ) {

    if(
      !this.isSubscriptions( stack ) ||
      !this.isCategory( category )
    ) {

      return false;

    }
  
    // UNSORTED cannot be updated!
    let catUnsorted = this.getCategoryByName( stack, "unsorted" );
  
    if( null === catUnsorted || false === catUnsorted ) {
      return false;
    }
  
    let cat = this.getCategoryById( stack, category.id );
  
    if( null === cat || false === cat ) {
      return false;
    }
  
    // UNSORTED cannot be updated!
    if( catUnsorted.id === cat.id ) {
      return false;
    }

    let catIndex = this.getCategoryIndexById( stack, cat.id );

    // Update cetrain, HERE DEFINED properties
    // @todo check SOME for UNIQUEness!
    stack.data[ catIndex ].name = category.name;
  
    return true;

  },

  /**
   * Update Subscription
   *
   * @returns 
   */
  updateSubscription( stack, subscription ) {

    if(
      !this.isSubscriptions( stack ) ||
      !this.isSubscription( subscription )
    ) {

      return false;

    }

    // SubscriptionsResult{ categoryIndex, subsciptionIndex, subscription }
    let sub = this.getSubscriptionById( stack, subscription.id );
  
    if( null === sub || false === sub ) {
      return false;
    }
  
    // Make sure the data is valid?! => Unique NAME in stack!?
    // .update only executes the update?!
    sub.subscription.update( subscription );

    // Update stack
    stack.data[ sub.categoryIndex ].subscriptions[ sub.subscriptionIndex ] = sub.subscription;
  
    return true;

  },

  /**
   * Move Subscription to another category
   *
   * @returns "Subscription" object or BOOLEAN
   */
  moveSubscription( stack, subscription, toCategoryId ) {

    if(
      !this.isSubscriptions( stack ) ||
      !this.isSubscription( subscription )
    ) {

      return false;

    }

    let toCatIndex   = this.getCategoryIndexById( stack, toCategoryId );
  
    if( null === toCatIndex || false === toCatIndex || toCatIndex < 0 ) {
      return false;
    }
  
    let sub = this.getSubscriptionById( stack, subscription.id );
  
    if( null === sub || false === sub ) {
      return false;
    }

    let fromCatIndex  = sub.categoryIndex;
  
    if( fromCatIndex === toCatIndex ) {
      return false;
    } 
  
    // Remove from old category
    stack.data[ fromCatIndex ].subscriptions.splice( sub.subscriptionIndex, 1 );

    // Add to new category
    // categories[ toCatIndex ].subscriptions.push( sub.subscription );
    let status = this.addNewSubscription( stack, sub.subscription, stack.data[ toCatIndex ].id );

    return status;
  
  },

  /**
   * Delete Category
   *
   * @returns ??? or FALSE
   */
  deleteCategory( stack, categoryId ) {
  
    // @todo wiederkehrende Funktion is HELPER auslagern
    if( !this.isSubscriptions( stack ) ) {
      return false;
    }

    // UNSORTED cannot be deleted
    let catUnsorted       = this.getCategoryByName( stack, "unsorted" );
    let catUnsortedIndex  = this.getCategoryIndexByName( stack, "unsorted" );
  
    if(
      catUnsorted.id === categoryId ||
      false === catUnsortedIndex ||
      catUnsortedIndex < 0
    ) {
      return false;
    }

    // @todo Move all subscriptions to UNSORTED!!
    // ONLY THEN delete the category
  
    let catIndex = this.getCategoryIndexById( stack, categoryId );

    if( false === catIndex || catIndex < 0 ) {
      return false;
    }

    // Move all subscriptions from the selected category to "UNSORTED"
    let allSubsHaveBeenMoved = true;

    stack.data[ catIndex ].subscriptions.forEach( function( sub ) {

      let status_subMoved = this.moveSubscription( stack, sub, catUnsortedIndex.id );

      if( status_subMoved === false ) {

        // @todo keep tracke of MOVED subs and roll back? On confirmation/choice?
        allSubsHaveBeenMoved = false;

      }
      
    });
    
    // Only if all subs have been moved you can delete the given cateory.
    if( allSubsHaveBeenMoved ) {

      stack.data.splice( catIndex, 1 );

      return true;

    }
    else {

      return false;

    }

  },

  /**
   * Delete Subscription
   * @returns BOOLEAN
   */
  deleteSubscription( stack, subscriptionId ) {

    if( !this.isSubscriptions( stack ) ) {

      return false;

    }

    let sub = this.getSubscriptionById( stack, subscriptionId );
  
    if( false === sub ) {
      return false;
    }

    stack.data[ sub.categoryIndex ].subscriptions.splice( sub.subscriptionIndex, 1 );
  
    return true;

  }

};
