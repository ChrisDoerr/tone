
TOne.Console = {
  el : document.getElementById("TOne-Console")
};

TOne.Console.clear = function() {
  
  "use strict";
  
  TOne.Console.el.innerHTML = "";
  
};

TOne.Console.write = function( argTitle, argMessage, argType ) {
  
  "use strict";
  
  var message = ( "string" === typeof argMessage )  ? argMessage.trim() : "";
  var title   = ( "string" === typeof argTitle )    ? argTitle.trim()   : "";
  var type    = ( "string" === typeof argType )     ? argType.toLowerCase().trim()    : "";
  var time    = new Date();
  var el      = {};
  
  if( message === "" ) {
    return;
  }
  
  el.container = document.createElement("div");
  el.container.setAttribute( "class", "tone-body-console-message" );
  
  if( type !== "" ) {
    
    el.container.setAttribute( "data-type", type );
    
  }
  
  el.time = document.createElement("div");
  el.time.setAttribute( "class", "tone-body-console-message-time" );

  el.timeDate = document.createTextNode( time.getFullYear() + "-" + ( "0" + time.getMonth() ).slice(-2) + "-" + ( "0" + time.getDate() ).slice(-2) );
  
  el.timeTime = document.createElement("span");
  el.timeTime.innerText = " (" + ( "0" + time.getHours() ).slice(-2) + ":" + ( "0" + time.getMinutes() ).slice(-2) + ":" + ( "0" + time.getSeconds() ).slice(-2) + "." + ( "00" + time.getMilliseconds() ).slice(-3) + ")";
  
  el.time.appendChild( el.timeDate );
  el.time.appendChild( el.timeTime );
  el.container.appendChild( el.time );
  
  if( title !== "" ) {
    
    el.title = document.createElement("div");
    el.title.setAttribute( "class", "tone-body-console-message-title" );
    el.title.innerText = "[" + title + "]";
    
    el.container.appendChild( el.title );
    
  }
  
  el.message = document.createElement("div");
  el.message.setAttribute( "class", "tone-body-console-message-body" );
  el.message.innerHTML = message;
  
  el.container.appendChild( el.message );
  
  
  // TOne.Console.el.prepend( el.container );
  
};

TOne.Console.success = function( title, message ) {
  
  "use strict";
  
  TOne.Console.write( title, message, "success" );
  
};

TOne.Console.error = function( title, message ) {
  
  "use strict";
  
  TOne.Console.write( title, message, "error" );
  
};
