
function sortObjects ( arrObjects, property, isString ) {
  
  "use strict";
  
  var _isString  = ( "boolean" === typeof isString ? isString : false );

  // Flat/Deep copy
  var result    = arrObjects.slice(0);
  
  result.sort(
    function( a, b ) {
      
      if( "undefined" === typeof a[ property ] || "undefined" === typeof b[ property ] ) {
        return 0;
      }
      
      let _a = ( _isString ? a[ property ].trim().toLowerCase() : a[ property ] );
      let _b = ( _isString ? b[ property ].trim().toLowerCase() : b[ property ] );
      
      if( _a < _b ) {
        return -1;
      }
      else if( _a > _b ) {
        return 1;
      }
      else {
        return 0;
      }
      
    }
  );
  
  return result;
  
}


function sanatizePageNr( pageNumber ) {
  
  "use strict";
  
  var pageNr = ( "number" === typeof pageNumber || "string" === typeof pageNumber ? Math.max( 1, parseInt( pageNumber, 10 ) ) : 1 );

  return pageNr;
  
}


function sortObjectsByProperty( objects, property, order ) {
  
  "use strict";

  // Paramter "objects" HAS TO BE an array...
  if( !Array.isArray( objects ) ) {
    return objects;
  }
  
  let sortingOrder  = ( ( "string" === typeof order && order.trim().toLowerCase() === "desc" ) ? "desc" : "asc" );
  let i             = 0;
  let dimObjects    = objects.length;
  
  for( i = 0; i < dimObjects; i += 1 ) {
    
    // ... of objects...
    if(
      "object" !== typeof objects[i] ||
      // that also ALL have the property to be sorted by.
      "undefined" === typeof objects[i][ property ]
    ) {
      return objects;
    }
    
  }
  
  // Make a flat/deep copy so the sorting will not effect the original array.
  let copy            = objects.slice(0);
  
  // Now that we've established that do the actual sorting.
  // The dynamic function is required in order to use the external "property" variable inside.
  copy.sort( function( obj1, obj2 ) {
    
    let prop1       = obj1[ property ];
    let prop2       = obj2[ property ];
    
    if( "string" === typeof prop1 ) {
      prop1         = prop1.trim().toLowerCase();
    }
    
    if( "string" === typeof prop2 ) {
      prop2         = prop2.trim().toLowerCase();
    }

    if( prop1 < prop2 ) {

      return ( sortingOrder === "asc" ? -1 : 1 );

    }
    else if( prop1 > prop2 ) {

      return ( sortingOrder === "asc" ? 1 : -1 );

    }
    else {

      return 0;

    }

  });
  
  return copy;
  
}

function addLeading( value, char, length ) {

  value       = value.toString();
  length      = Math.max( 0, parseInt( length, 10 ) );

  if( value.length >= length ) {
    return value;
  }

  let newVal  = value;
  let i       = 0;

  for( i = value.length; i < length; i += 1 ) {

    newVal = char + newVal;

  }

  return newVal;

}

function leadingDateTime( value ) {

  return addLeading( value, "0", 2 );

}

function getDateTimeString() {

  let now             = new Date();

  let day             = now.getDate();
  let month           = ( now.getMonth() +1 );
  let year            = now.getFullYear();

  let hours           = now.getHours();
  let minutes         = now.getMinutes();
  let seconds         = now.getSeconds();

  let dateTimeString  = "";

  dateTimeString      += year;
  dateTimeString      += leadingDateTime( month );
  dateTimeString      += leadingDateTime( day );
  dateTimeString      += leadingDateTime( hours );
  dateTimeString      += leadingDateTime( minutes );
  dateTimeString      += leadingDateTime( seconds );

  return dateTimeString;

}

function stripHTML( fromString ) {

  var el = document.createElement("div");
  
  el.innerHTML = fromString;
  
  return ( el.textContent || el.innerText || "" );

}

function ucFirst( text ) {

  return ( "string" === typeof text ? ( text.charAt(0).toUpperCase() + text.slice( 1 ) ) : text );

}

function prepStringValue( value ) {

  return value.trim().toLowerCase();

}

function prepNumericValue( value ) {

  return parseInt( value, 10 );

}


export {
  sortObjects,
  sortObjectsByProperty,
  sanatizePageNr,
  getDateTimeString,
  stripHTML,
  ucFirst,
  prepStringValue,
  prepNumericValue
};
