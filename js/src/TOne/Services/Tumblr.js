
TOne.Services.Tumblr = {};

TOne.Services.Tumblr.loadFeed = function( argBlogname, argPageNr ) {
  
  "use strict";
  
  var blogname  = argBlogname;
  var pageNr    = ( "number" === typeof argPageNr ) ? parseInt( argPageNr, 10 ) : 1;
  
  if( pageNr < 1 ) {
    pageNr = 1;
  }
  

  // TOne.Console.write( "Loading Tumbrl Blog", "Blogname: " + blogname + "<br />Page Nr: " + pageNr );

  var apiUrl_getPosts = "https://api.tumblr.com/v2/blog/" + blogname + ".tumblr.com/posts?api_key=" + TOne.config.tumblrCustomerKey + "&notes_info=true&reblog_info=true&offset=" + ( ( pageNr -1 ) * 20 );

  https.get( apiUrl_getPosts, function( resp ) {
    
    let data = '';

    // A chunk of data has been recieved.
    resp.on( "data", function( chunk ) {

      data += chunk;

    });

    // The whole response has been received. Print out the result.
    resp.on( "end", function() {
      
      var res     = JSON.parse( data );
// console.log(res);      
      if( res.meta.status !== 200 ) {
        
        // TOne.Console.error( ... )
        app.updateLoadingStatus( false );

        // console.log( res.meta.status + " - " + res.errors[0].title, res.errors[0].detail + "<br />Service: Tumblr<br />Blogname: " + blogname + "<br />Page Nr: " + pageNr );

        app.addConsoleMessage(
          "HTTP Status " + res.meta.status,
          res.errors[0].detail,
          "error",
          {
            blogname  : blogname,
            service   : "Tumblr",
            pageNr    : pageNr
          }
        );

        return;
        
      }
      
      // var images  = TOne.Services.Tumblr.parsePosts( res.response.posts );
      
      // @todo better pass data to an app method!!!
      let items = generateGalleryItemsFromTumblrResponse( res );
      
      app.$data.gallery.items = items;
      app.updateLoadingStatus( false );
      
      // @todo dont generate and insert in "parsePosts", just collect image data and send it to a VIEW
  
    });

  }).on( "error", function( err ) {
    
    console.log( "Error: " + err.message );
  
  });
  
};





TOne.Services.Tumblr.parseTextPost = function( post ) {
  
  'use strict';
  
  var tmp       = document.createElement('div'),
      re        = new RegExp( "(.*)_[0-9]+\.(jpg|jpeg|png)", "i" ),
      figure    = {},
      dimFigure = 0,
      i         = 0,
      img       = {},
      imageUrl  = '',
      images    = [],
      matches   = {};

  tmp.innerHTML = post.body;

  figure    = tmp.querySelectorAll('figure.tmblr-full');
  
  dimFigure = figure.length;
  
  if( dimFigure < 1 ) {
    return false;
  }
  
  for( i = 0; i < dimFigure; i += 1 ) {

    // @todo oder auch ALLE Bilder??
    img    	  = figure[0].querySelector('img');
    
    if( null === img ) {
      continue;
    }

    imageUrl  = img.getAttribute( 'src' );

    matches   = imageUrl.match( re );
    // matches[1] = "raw" url (ohne size) und ohne Dateieindung
    // matches[2] = Dateiendung

    if( null === matches || matches.length < 3 ) {
      continue;
    }

  	images.push({
      thumbnail : matches[1] + '_250.' + matches[2],
      original  : matches[1] + '_1280.' + matches[2],
      timestamp : post.timestamp,
      blogname  : post.blog_name
    });
  
  }
  
  return ( images.length > 0 ? images : false );

};

TOne.Services.Tumblr.parseImagePost = function( post ) {
  
  'use strict';

  var images = [];

  if( post.photos.length < 1 ) {
    return false;
  };

  post.photos.forEach( function( image ) {

    var reSupportedFiles  = /(jpeg|jpg|png)$/i,
        dimAlt            = 0,
        i                 = 0,
        original          = '',
        thumbnail         = '';

    if( image.original_size.url.match( reSupportedFiles ) ) {
        
      original  = image.original_size.url;

      dimAlt    = image.alt_sizes.length;
      
      for( i = 0; i < dimAlt; i += 1 ) {
      
        if( image.alt_sizes[i].width === 250 ) {
          thumbnail = image.alt_sizes[i].url;
          break;
        }
        
        // next best thumb size, assume the one next below 250
        if( image.alt_sizes[i].width < 250 ) {
          thumbnail = image.alt_sizes[i].url;
          break;
        }
      
      }

      images.push({
        thumbnail : thumbnail,
        original  : original,
        timestamp : post.timestamp,
        blogname  : post.blog_name
      });
    
    }

  });

  return ( images.length > 0 ? images : false );

};


TOne.Services.Tumblr.parsePosts = function( posts, argIsSearch ) {
  
  'use strict';
  
  var isSearch = ( "boolean" === typeof argIsSearch ? argIsSearch : false );
  var images    = [];
  var result    = {};
  var reblogs   = "";
  var el        = {};
  
  if( posts.length < 1 ) {
    console.log( "Error: No Posts!" );
    return;
  }
  
  // Collect images
  posts.forEach( function( post ) {

    if( post.type === "photo" ) {
      
      result = TOne.Services.Tumblr.parseImagePost( post );
      
      if( false !== result ) {
        
        images.push.apply( images, result );
        
      }
      
    }
    else if( post.type === "text" ) {
      
      result = TOne.Services.Tumblr.parseTextPost( post );
      
      if( false !== result ) {
        
        images.push.apply( images, result );
        
      }
      
    }
    else {
      return;
    }
    
    if(
      post.note_count > 0
      && "object" === typeof post.notes
      && post.notes.length > 0
      ) {

      // Also note down the REBLOGs
      // Notes ist leider super limitert!
      post.notes.forEach( function( note ) {
      
        if( note.type === "reblog" ) {
        
          reblogs += note.blog_name + ",";
        
        }
      
      });
  
    }
    
  });
  
  if( images.length < 1 ) {
    console.log( "KEINE BILDER GEFUNDEN" );
    return;
  }

  // reset
  TOne.el.gallery.innerHTML = '';
  
  images.forEach( function( image ) {
    
/*
    el.img = document.createElement('img');
    
    el.img.setAttribute( 'src', image.thumbnail );
    el.img.setAttribute( 'data-original', image.original );
//    el.img.setAttribute( 'data-postid', post.id );
//    el.img.setAttribute( 'data-reblogs', reblogs );   // @todo fix this, doesnt make sense?!
    el.img.setAttribute( 'class', 'thumbnail' );

    el.img.addEventListener( 'click', TOne.handleClick_viewOriginal );

    TOne.el.main.appendChild( el.img );
*/

    el.item = document.createElement("div");
    el.item.setAttribute( "class", "tone-thumbnails-item" );
    
    el.img = document.createElement("img");
    el.img.setAttribute( "src", image.thumbnail );
    el.img.setAttribute( "data-original", image.original );
    el.img.setAttribute( "data-timestamp", image.timestamp );
    el.img.addEventListener( "click", TOne.App.handleClick.viewOriginal );
    
    el.item.appendChild( el.img );
    
    TOne.el.gallery.appendChild( el.item );

  });
  
};


TOne.Services.Tumblr.go = function( argBlogname ) {
  
  "use strict";
  
  var blogname = ( "string" === typeof argBlogname ? argBlogname.trim() : "" );
  
  if( blogname === "" ) {
    return;
  }

  TOne.Services.Tumblr.loadFeed( blogname );
  
};


// @todo also implement Pagination for/through search results + show BLOGNAMES > clickable and/or "liste führen" in einer Art Clip Board

TOne.Services.Tumblr.search = function( terms ) {
  
  'use strict';
  
  var terms           = encodeURIComponent( terms.trim() );
  var apiUrl_search   = "https://api.tumblr.com/v2/tagged?tag=" + terms  + "&api_key=" + TOne.config.tumblrCustomerKey;

  fetch( apiUrl_search )
    .then( function( response ) {

      return response.json();

    }).then( function( jsonData ) {
      
      if( jsonData.meta.status === 200 && jsonData.response.length > 0 ) {
        
        var images = TOne.Services.Tumblr.parsePosts( jsonData.response, true );

      }
      
    }).catch( error => console.log( error ) );
  
};
