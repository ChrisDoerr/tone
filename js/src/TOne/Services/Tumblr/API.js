import { sanatizePageNr } from "../../Helpers.js";

import TumblrBlogResponse from "./BlogResponse.js";
import TumblrBlogInfoResponse from "./BlogInfoResponse.js";
import TumblrSearchResponse from "./SearchResponse.js";

/**
 * @requires Node.js module "https"
 */
export default class TumblrAPI {
  
  constructor( config ) {
    
    var conf          = ( "object" === typeof config ? config : {} );
    
    this.customerKey  = ( "string" === typeof conf.customerKey ? conf.customerKey.trim() : "" );

    this.baseUrl      = "https://api.tumblr.com/v2/";
    
  }

  buildUrlParameters( data ) {
    
    if( "object" !== typeof data ) {
      return "";
    }
    
    let urlParameters = "";
    
    Object.keys( data ).forEach( function( key ) {
      
      urlParameters += key + "=" + encodeURIComponent( data[ key ] ) + "&";
      
    });
    
    // Cut off the last ampersand
    return urlParameters.substring( 0, ( urlParameters.length -1 ) );
    
  }
  
  calculateOffset( pageNumber ) {
    
    let pageNr = sanatizePageNr( pageNumber );
    
    return ( ( pageNr -1 ) * 20 );
    
  }

  // @requires Node.js module "https"
  loadUrl( url )  {

    return new Promise( function( resolve, reject ) {
  
      if( "string" !== typeof url || url.trim() === "" ) {
        reject( "Empty URL paramter!" );
      }
    
      var request = https.get( url, function( res ) {
    
        let data = "";
    
        res.on( "data", function( chunk ) {
      
          data += chunk;
      
        });
    
        res.on( "end", function() {

          let response = {};
      
          try {
        
            response = JSON.parse( data );
     
          }
          catch( err ) {
        
            response.error = err;
        
          }
        
          resolve( response );
      
        });
      
        res.on( "error", function( err ) {
        
          reject( err );
        
        });
      
      });
    
      request.on( "error", function( err ) {

        reject( err );

      });
  
      request.end();
  
    });

  }

  async getSearchResults( searchTerms, pageNumber ) {
  
    let terms         = ( "string" === typeof searchTerms ? searchTerms.trim() : "" );
    let pageNr        = sanatizePageNr( pageNumber );
    
    let offset        = this.calculateOffset( pageNr );

    let params        = {
      api_key : this.customerKey,
      tag     : terms,
      offset  : offset
    };
    
    let requestUrl    = this.baseUrl + "tagged?" + this.buildUrlParameters( params );
    
    try {
      
      const response  = await this.loadUrl( requestUrl );
      // console.log( response );
      
      const searchResponse = new TumblrSearchResponse( response );
      // console.log( searchResponse );

      return searchResponse;

    }
    catch( err ) {
      
      // @todo proper...
      console.log( err );
      
    }
  
  }

  async getBlogInfo( blogName ) {

    let blogname = ( "string" === typeof blogName ? blogName.trim() : "" );
    
    let params = {
      api_key     : this.customerKey,
      notes_info  : "true"
    };

    let requestUrl         = this.baseUrl + "blog/" + blogname + ".tumblr.com/posts?" + this.buildUrlParameters( params );

    try {
   
      const response      = await this.loadUrl( requestUrl );
      // console.log( response );

      const blogResponse  = new TumblrBlogInfoResponse( response );
      // console.log( blogResponse );

      return blogResponse;
  
    }
    catch( err ) {
    
      console.log( err );
    
    }

  }

  async getPosts( blogName, pageNumber ) {
  
    let blogname           = ( "string" === typeof blogName ? blogName.trim() : "" );
    let pageNr             = sanatizePageNr( pageNumber );

    let offset             = this.calculateOffset( pageNr );

    let params             = {
      api_key     : this.customerKey,
      notes_info  : "true",
      reblog_info : "true",
      offset      : offset
    };
  
    let requestUrl         = this.baseUrl + "blog/" + blogname + ".tumblr.com/posts?" + this.buildUrlParameters( params );
  
    try {
   
      const response      = await this.loadUrl( requestUrl );
    
      const blogResponse  = new TumblrBlogResponse( response );
      // console.log( blogResponse );

      return blogResponse;
  
    }
    catch( err ) {
    
      console.log( err );
    
    }

  }

}
