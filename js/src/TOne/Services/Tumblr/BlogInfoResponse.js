import { parsePhotoItems } from "./Helpers.js";
import TumblrResponse from "./Response.js";

export default class TumblrBlogInfoResponse extends TumblrResponse {
  
  constructor( responseObject ) {
    
    super( responseObject );
    
    this.info = this.parseResponse( responseObject );
    
  }

  parseResponse( responseObject ) {

    let blogData = {
        blogname    : "",
        title       : ""
    };

    if(
      "object" !== typeof responseObject ||
      "object" !== typeof responseObject.response ||
      "object" !== typeof responseObject.response.blog
    ) {
      return blogData;
    }

    blogData.blogname   = responseObject.response.blog.name.trim();
    blogData.title      = responseObject.response.blog.title.trim();

    return blogData;
    
  }

}
