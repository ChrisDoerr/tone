import { parsePhotoItems } from "./Helpers.js";
import TumblrResponse from "./Response.js";

export default class TumblrBlogResponse extends TumblrResponse {
  
  constructor( responseObject ) {
    
    super( responseObject );
    
    this.photoItems = this.parseResponse( responseObject );
    
  }

  parseResponse( responseObject ) {

    if(
      "object" !== typeof responseObject ||
      "object" !== typeof responseObject.response ||
      "object" !== typeof responseObject.response.posts ||
      !Array.isArray( responseObject.response.posts ) ||
      responseObject.response.posts.length < 1
      
    ) {
      return [];
    }

    return parsePhotoItems( responseObject.response.posts );

  }

}
