import { sortObjects } from "../../Helpers.js";

function parsePhotoItems( items ) {

  if( "object" !== typeof items || !Array.isArray( items ) ) {
    return [];
  }

  var photoItems  = [];

  var i           = 0;
  var dimItems    = items.length;

  for( i = 0; i < dimItems; i += 1 ) {
    
    if( "string" !== typeof items[i].type || items[i].type !== "photo" ) {
      continue;
    }

    let date = ( "string" === typeof items[i].date ? items[i].date : Date.now().toDateString() );
    
    let item = {
        blogname    : ( "string" === typeof items[i].blog_name ? items[i].blog_name.trim() : "" ),
        date        : date.substring( 0, 10 ),
        stamp       : ( "string" === typeof items[i].timestamp ? items[i].timestamp.trim() : Date.now().timestamp ),
        reblogKey   : ( "string" === typeof items[i].reblog_key ? items[i].reblog_key : "" ),
        hasReblogs  : false,
        notes       : ( Array.isArray( items[i].notes ) ? items[i].notes : [] ),
        photos      : []
      };

    if(
      item.blogname === "" ||
      !Array.isArray( items[i].photos ) ||
      items[i].photos.length < 1
    ) {
      continue;
    }

    item.photos = parsePhotos( items[i].photos, 100 );

    // Only add item if the photos array is not empty.
    // That can happen, e.g. when it (only) contained "unwanted" file formats (here, (animated)GIFs)
    if( item.photos.length > 0 ) {
      
      photoItems.push( item );

    }
      
  }

  return photoItems;

}

function parsePhotos( photos, minWidth ) {
  
  "use strict";
  
  var items     = [];

  var _minWidth  = Math.max( 75, parseInt( minWidth, 10 ) );
  
  if( Array.isArray( photos ) ) {
    
    photos.forEach( function( photo ) {
      
      let photoItem = {
        originalUrl   : ( "object" === typeof photo.original_size && "string" === typeof photo.original_size.url  ? photo.original_size.url.trim() : "" ),
        thumbnailUrl  : ""
      };

      // Only allow certain image file formats
      // Here, NO gifs!
      // @todo via config/argument would be much better!
      if( !photoItem.originalUrl.match( /\.(jpg|jpeg|png|webm)$/i ) ) {
        return;
      }
      
      if( !Array.isArray( photo.alt_sizes ) || photo.alt_sizes.length < 1 ) {
        return;
      }
        
      let i             = 0;
      // Sort the alternative sizes by "width"
      let altSizes      = sortObjects( photo.alt_sizes, "width" );
      let dimAltSizes   = photo.alt_sizes.length;
      let thumbnailUrl  = "";
      
      for( i = 0; i < dimAltSizes; i += 1 ) {

        // Save the current URL
        // as long as the current width is LOWER OR EQUAL than "minWidth"
        // console.log( altSizes[i].width, _minWidth );
        if( altSizes[i].width <= _minWidth && "string" === typeof altSizes[i].url ) {

          thumbnailUrl = altSizes[i].url;

        }
        // If the current width is bigger, quit the for loop for good.
        // This SHOULD leave you with the closest, best value!?
        else {
          break;
        }
          
      }

      photoItem.thumbnailUrl = thumbnailUrl.trim();

      if( photoItem.originalUrl !== "" && photoItem.thumbnailUrl !== "" ) {
        
        items.push( photoItem );
        
      }
      
    });
    
  }
  
  return items;
  
}

export {
  parsePhotoItems,
  parsePhotos
};
