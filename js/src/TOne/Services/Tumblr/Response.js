
export default class TumblrResponse {

  constructor( responseObject) {
  
    var _response   = ( "object" === typeof responseObject ? responseObject : {} );

    this.photoItems = [];

    this.meta       = this.parseMeta( _response );

  }

  parseMeta( response ) {

    let meta = {
      msg     : "",
      status  : -1
    };

    if( "object" === typeof response.meta ) {

      if( "string" === typeof response.meta.msg ) {
        meta.msg = response.meta.msg.trim();
      }

      if( "number" === typeof response.meta.status ) {
        // >= 0
        meta.status = Math.max( 0, parseInt( response.meta.status, 10 ) );
      }
    
    }
   
    return meta;
  
  }

}
