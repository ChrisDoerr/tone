import { parsePhotoItems } from "./Helpers.js";
import TumblrResponse from "./Response.js";

export default class TumblrSearchResponse extends TumblrResponse {
  
  constructor( responseObject ) {
    
    super( responseObject );
    
    this.photoItems = this.parseResponse( responseObject );
    
  }

  parseResponse( responseObject ) {

    if( "object" !== typeof responseObject || "object" !== typeof responseObject.response ) {
      return [];
    }
    
    return parsePhotoItems( responseObject.response );
    
  }

}
