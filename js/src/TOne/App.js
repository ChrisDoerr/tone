/**
 * [TOne/App.js]
 */

 /*jslint
  browser:true,
  devel:true,
  for:true,
  this:true,
  single:true,
  long:true,
  white:true,
  bitwise:true
*/

/*global
  window,
  TOne,
  fs,
  Image
*/


TOne.App = {
  handleClick : {}
};

TOne.App.parseDOM = function() {
  
  "use strict";
  
  TOne.el = {};
  
  TOne.el.header            = document.getElementById("TOne-Header");
  TOne.el.body              = document.getElementById("TOne-Body");
  
  TOne.el.btnAbout          = document.getElementById("TOne-Header-Logo-Text");
  // TOne.el.btnAbout.addEventListener( "click", function() { TOne.loadPage( "about" ); } );
  TOne.el.btnSettings       = document.getElementById("TOne-Header-Logo-Settings");
  // TOne.el.btnSettings.addEventListener( "click", function() { TOne.loadPage( "settings" ); } );
  
  TOne.el.btnEditCategories = document.getElementById("TOne-BtnEditCategories");
  TOne.el.btnAddCategory    = document.getElementById("TOne-BtnAddCategory");
  
  TOne.el.categories        = document.getElementById("TOne-Header-Categories-List");
  
  TOne.el.sidebarTitle      = document.getElementById("TOne-Sidebar-Title");
  TOne.el.subscriptions     = document.getElementById("TOne-Sidebar-Subscriptions");
  
  TOne.el.main              = document.getElementById("TOne-Body-Main");
  
  TOne.el.console           = document.getElementById("TOne-Body-Console");

  TOne.el.original          = document.getElementById('TOne-OriginalImage');
  
  TOne.el.contextMenu       = document.getElementById("TOne-ContextMenu");
  
};

TOne.App.sortByTitle = function( a, b ) {
  
  "use strict";
  
  if( a.title > b.title ) {
    return 1;
  }
  
  if( a.title < b.title ) {
    return -1;
  }
  
  return 0;
  
};

TOne.App.parseCategories = function( cats ) {
  
  "use strict";
  
  var categories      = [];
  var dimCategories   = cats.length;
  var i               = 0;
  var category        = {};
  
  for( i = 0; i < dimCategories; i += 1 ) {
    
    category      = new TOne.Category.Object( cats[i] );
    
    categories.push( category );
    
  }
  
  return categories;
  
};

TOne.App.loadConfiguration = function() {
  
  "use strict";

  var contents    = fs.readFileSync( "data/config.json" );
  var configData  = JSON.parse( contents );
  
  TOne.config     = configData;
  
};

TOne.App.loadCategories = function() {
  
  "use strict";
  
  var contents        = fs.readFileSync( "data/categories.json" );
  var categoriesData  = JSON.parse( contents );
  
  TOne.categories     = TOne.App.parseCategories( categoriesData.categories );

};

TOne.App.parseSubscriptions = function( subs ) {
  
  "use strict";
  
  var subscriptions     = [];
  var dimSubscriptions  = subs.length;
  var i                 = 0;
  var subscription      = {};
  
  for( i = 0; i < dimSubscriptions; i += 1 ) {
    
    subscription = new TOne.Subscription.Object( subs[i] );
    
    subscriptions.push( subscription );
    
  }
  
  return subscriptions;
  
};


TOne.updateCategorySubscriptionsCount = function() {
  
  "use strict";
  
  var dimCats       = TOne.categories.length;
  var i             = 0;
  var n             = 0;
  var subscriptions = {};
  var dimSub        = 0;

  for( i = 0; i < dimCats; i += 1 ) {
    
    subscriptions = TOne.App.getSubscriptionsByCategoryId( TOne.categories[i].id );
    dimSub        = 0;
    
    for( n = 0; n < subscriptions.length; n += 1 ) {
      
      if( subscriptions[n].status === "ok" ) {
        dimSub += 1;
      }
      
    }

    TOne.categories[i].setSubscriptions( dimSub );
    
  }
  
};


TOne.App.loadSubscriptions = function() {
  
  "use strict";

  var contents          = fs.readFileSync( "data/subscriptions.json" );
  var subscriptionsData = JSON.parse( contents );

  TOne.subscriptions    = TOne.App.parseSubscriptions( subscriptionsData.subscriptions );
  
  // Update subscriptions count
  TOne.updateCategorySubscriptionsCount();

};

TOne.App.updateUICategories = function() {
  
  "use strict";
  
  var categories = TOne.categories.slice();
  // @todo sort by order

  var dimCategories = categories.length;
  var i = 0;
  
  // reset
  TOne.el.categories.innerHTML = "";
  
  for( i = 0; i < dimCategories; i += 1 ) {
    
    TOne.el.categories.appendChild( categories[i].el.li );
    
  }
  
};

TOne.App.getCategoryById = function( argCatId ) {
  
  "use strict";
  
  var dimCats   = TOne.categories.length;
  var i         = 0;
  
  for( i = 0; i < dimCats; i += 1 ) {
    
    if( TOne.categories[i].id === argCatId ) {
      
      return TOne.categories[i];
      
    }
    
  }
  
  return false;
  
};

TOne.App.getSubscriptionsByCategoryId = function( argCatId ) {
  
  "use strict";
  
  var dimSubscriptions  = TOne.subscriptions.length;
  var i                 = 0;
  var subscriptions     = [];
  
  for( i = 0; i < dimSubscriptions; i += 1 ) {
    
    if( TOne.subscriptions[i].category === argCatId ) {
      
      subscriptions.push( TOne.subscriptions[i] );
      
    }
    
  }
  
  return subscriptions;
  
};

TOne.App.updateUISubscriptions = function( categoryId ) {
  
  "use strict";
  
  // reset
  TOne.el.subscriptions.innerHTML = "";
  
  var category      = TOne.App.getCategoryById( categoryId );
  
  if( false === category ) {
    return false;
  }

  // Set sidebar title
  TOne.el.sidebarTitle.innerText = category.name;
  
  // Set App state category
  TOne.App.state.category = categoryId;
  // @todo save state!
  
  var subscriptions = TOne.App.getSubscriptionsByCategoryId( categoryId );
  var dimSubs       = subscriptions.length;
  var i             = 0;

  // Sort by title
  subscriptions.sort( TOne.App.sortByTitle );
  
  for( i = 0; i < dimSubs; i += 1 ) {
    
    TOne.el.subscriptions.appendChild( subscriptions[i].el.li );
    
  }
  
};

TOne.App.resizeBody = function() {
  
  "use strict";

  var diff          = ( window.innerHeight - TOne.el.header.offsetHeight );

  TOne.el.body.style.height = ( ( diff < TOne.el.body.offsetHeight ) ? diff : TOne.el.body.offsetHeight ) + "px";
  
};

TOne.App.handleClick.viewOriginal = function( event ) {
  
  "use strict";

  event.preventDefault();

  TOne.App.viewOriginal( this.getAttribute( "data-original" ), this.getAttribute( "data-timestamp" ) );
  
};

TOne.App.closeOriginal = function() {
  
  'use strict';
  
  TOne.el.original.innerHTML = "";
  TOne.el.original.classList.add( "hidden" );
  
};


TOne.App.viewOriginal = function ( url, timestamp ) {
  
  "use strict";

  var img     = new Image();

  img.addEventListener( "load", function() {
    
    TOne.App.resizeOriginalWrapper();

    var elImg     = document.createElement("img");
    var offsetTop = ( window.innerHeight > img.height ? Math.floor( ( window.innerHeight - img.height ) / 2 ) : 0 );
  
    if( offsetTop > 0 ) {
      
      elImg.style.marginTop = offsetTop + 'px';
    
    }

    elImg.src = url;
    elImg.setAttribute( "data-timestamp", timestamp );
    
    elImg.addEventListener( "click", TOne.App.closeOriginal );
    elImg.addEventListener( "contextmenu", TOne.ContextMenu.viewOriginal );
  
    TOne.el.original.innerHTML = '';
    TOne.el.original.appendChild( elImg );
    
    TOne.el.original.classList.remove( 'hidden' );
    
  });
  
  img.src = url;
  
};

TOne.App.resizeOriginalWrapper = function() {
  
  "use strict";
  
  var viewport = {
    width   : window.innerWidth,
    height  : window.innerHeight
  };
  
  TOne.el.original.style.width      = viewport.width + 'px';
  TOne.el.original.style.height     = viewport.height + 'px';
  TOne.el.original.style.lineHeight = viewport.height + 'px';
  
};

TOne.App.generateServiceForm = function() {
  
  "use strict";
  
  var el = {};
  // @todo via config/detection
  var services = [
    {
      slug    : "Tumblr",
      domain  : "tumblr.com"
    },
    {
      slug    : "Pinterest",
      domain  : "pinterest.com"
    }
  ];
  
  el.form = document.createElement("form");
  el.form.setAttribute( "id", "TOne-Body-Main-Form" );
  el.form.setAttribute( "action", "javascript:void(0);" );

    el.schema = document.createTextNode("https://");
  
    el.input = document.createElement("input");
    el.input.setAttribute( "type", "text" );
    el.input.setAttribute( "id", "TOne-Main-Body-Form-Data" );
    el.input.setAttribute( "value", "example" );
  
    el.select = document.createElement("select");
    el.select.setAttribute( "id", "TOne-Main-Body-Form-ServiceUrl" );
    el.select.setAttribute( "size", "1" );
  
    services.forEach( function( service ) {
      
      el.option = document.createElement("option");
      el.option.setAttribute( "value", service.slug );
      el.option.innerText = "." + service.domain;
      
      el.select.appendChild( el.option );
      
    });
    
    el.btnGo = document.createElement("button");
    el.btnGo.innerText = "Go";
    el.btnGo.addEventListener( "click", function() {
      
      var serviceModule = TOne.App.getServiceModule( TOne.el.serviceForm.select.value );
      var userInput     = TOne.el.serviceForm.input.value;
      
      if( "function" === typeof TOne.Services[ serviceModule ].go ) {

        TOne.Services[ serviceModule ].go( userInput );

      }
      else {
        
        TOne.Console.error( "Service", "Service method .go() missing." );
        
      }

    });
  
    el.btnSearch = document.createElement("button");
    el.btnSearch.innerText = "Search";
    el.btnSearch.addEventListener( "click", TOne.App.handleClick.search );
    
    el.btnSubscribe = document.createElement("button");
    el.btnSubscribe.innerText = "Subscribe";
    el.btnSubscribe.addEventListener( "click", function() { console.log( "Click: Subscribe" ); } );
  
  el.form.appendChild( el.schema );
  el.form.appendChild( el.input );
  el.form.appendChild( el.select );
  el.form.appendChild( el.btnGo );
  el.form.appendChild( el.btnSearch );
  el.form.appendChild( el.btnSubscribe );
  
  return el;
  
};

TOne.App.viewGallery = function( argCreate ) {
  
  "use strict";
  
  var create = ( "boolean" === typeof argCreate ? argCreate : false );
  
  if( create ) {

    TOne.el.serviceForm = TOne.App.generateServiceForm();
    TOne.el.prevNextTop = TOne.App.createPrevNextNavigation();
    TOne.el.gallery     = TOne.App.generateGallery();

  }
  
  // Reset
  TOne.el.main.innerHTML = "";

  TOne.el.main.appendChild( TOne.el.serviceForm.form );
  TOne.el.main.appendChild( TOne.el.prevNextTop.nav );
  TOne.el.main.appendChild( TOne.el.gallery );
  
};

TOne.App.generateGallery = function() {
  
  var el = {};
  
  el.gallery = document.createElement("div");
  el.gallery.setAttribute( "id", "TOne-Body-Main-Gallery" );
  
  return el.gallery;

};

TOne.App.downloadImage = function( url, timestamp ) {
  
  "use strict";
  
  // @todo @next 
  // check/create tumblr blogname / year / month > download image file

  TOne.ContextMenu.close();
  
  console.log(
    url,
    timestamp,
    TOne.App.state.service,
    TOne.App.state.serviceModule,
    TOne.App.state.serviceUser,
    TOne.App.state.serviceUserCollection
  );
  
};

TOne.App.saveImageAs = function( url ) {
  
  "use strict";
  
  TOne.ContextMenu.close();

  var filename = url.split("/").pop();
  
  var a = document.createElement("a");
  a.setAttribute( "href", url );
  a.setAttribute( "download", filename );
  a.click();
  
};

TOne.App.keylistener = function( event ) {
  
  "use strict";

  var execLevel = 0;
  
  // ESC
  if( event.keyCode === 27 ) {
    
    // When CONTEXT Menu is open > close it
    if( !TOne.el.contextMenu.classList.contains("hidden" ) ) {
      
      TOne.ContextMenu.close();
      execLevel += 1;

    }

    // If ORIGINAL is open > close it
    if( execLevel === 0 && !TOne.el.original.classList.contains("hidden") ) {

      TOne.App.closeOriginal();
      execLevel += 1;

    }
    
  }
  
};


TOne.App.handleClick.btnPrev = function( event ) {
  
  "use strict";
  
  event.preventDefault();
  
  var pageNr = TOne.App.state.pageNr;
  
  pageNr -= 1;
  
  if( pageNr < 1 ) {
    return;
  }
  
  TOne.App.loadPage( pageNr );
  
};

TOne.App.handleClick.btnNext = function( event ) {
  
  "use strict";
  
  event.preventDefault();
  
  var pageNr = TOne.App.state.pageNr;
  
  pageNr += 1;
  
  TOne.App.loadPage( pageNr );
  
};

TOne.App.getServiceModule = function( argService ) {
  
  "use strict";
  
  var service       = argService.toLowerCase();
  var serviceModule = service.charAt(0).toUpperCase() + service.slice(1);
  
  return serviceModule;
  
};

TOne.App.loadPage = function( pageNr ) {
  
  "use strict";
  
  // @todo there has to be a better way | at least separate funtion?!
  // var service = TOne.App.state.service.charAt(0).toUpperCase() + TOne.App.state.service.slice(1);
  var serviceModule = TOne.App.state.serviceModule;

  TOne.App.state.pageNr = pageNr;
  
  // THis only works for TUMBLR -- servuceUserCategory
  TOne.Services[ serviceModule ].loadFeed( TOne.App.state.serviceUser, pageNr );
  
  TOne.el.prevNextTop.pageNr.innerText = pageNr;

};

TOne.App.handleClick.search = function( event ) {
  
  "use strict";
  
  event.preventDefault();
  
  var searchTerms = TOne.el.serviceForm.input.value.trim();
  // var service     = TOne.App.state.service.charAt(0).toUpperCase() + TOne.App.state.service.slice(1);
  var serviceModule = TOne.App.state.serviceModule;
  
  TOne.Services[ serviceModule ].search( searchTerms );
  
};


TOne.App.createPrevNextNavigation = function() {
  
  "use strict";
  
  var el = {};
  
  el.nav = document.createElement("nav");
  el.nav.setAttribute( "class", "tone-prevNextNav" );
  
  el.btnPrev = document.createElement("button");
  el.btnPrev.setAttribute( "title", "Previous Page" );
  el.btnPrev.innerHTML = "&#171; Prev";
  el.btnPrev.addEventListener( "click", TOne.App.handleClick.btnPrev );
  
  el.pageNr = document.createElement("div");
  el.pageNr.setAttribute( "class", "tone-prevNextNav-pageNr" );
  el.pageNr.innerText = TOne.App.state.pageNr;

  el.btnNext = document.createElement("button");
  el.btnNext.setAttribute( "title", "Next Page" );
  el.btnNext.innerHTML = "Next &#187;";
  el.btnNext.addEventListener( "click", TOne.App.handleClick.btnNext );
  
  el.nav.appendChild( el.btnPrev );
  el.nav.appendChild( el.pageNr );
  el.nav.appendChild( el.btnNext );
  
  return el;
  
};


TOne.App.init = function() {

  "use strict";
  
  TOne.App.loadConfiguration();
  
  TOne.App.parseDOM();

  TOne.App.state = {
    category              : 0,    // @todo set to 0 (unsorted) OR load from SAVED app state!!
    service               : "tumblr",
    serviceModule         : "Tumblr",
    serviceUser           : "",
    serviceUserCollection : "",
    pageNr                : 1
  };
  
  TOne.App.loadCategories();
  TOne.App.loadSubscriptions();
  
  TOne.App.updateUICategories();
  TOne.App.updateUISubscriptions( TOne.App.state.category );
  
  TOne.App.resizeBody();
  window.onresize = TOne.App.resizeBody;
  
  TOne.App.viewGallery( true );
  
  TOne.Console.clear();
  TOne.Console.write(
    "Welcome",
    "You are ready to go.",
    "success"
  );
  
  // "General" Key Listener
  document.addEventListener( "keyup", TOne.App.keylistener );
  
};
TOne.App.init();

