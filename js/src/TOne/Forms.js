/**
 * [TOne/App.js]
 */
/*jslint
  browser,
  devel,
  for,
  this,
  single,
  long,
  white,
  bitwise
*/
/*global window,TOne*/
/*jshint
  scripturl : true,
  esversion : 6
*/

TOne.Forms = {};

/*

// Form: Settings
var formSettings = new TOne.Forms.Object({
  title : "Settings",
  items : [
    {
      type      : "text",
      name      : "tumblrCustomerKey",
      label     : "Tumblr Customer Key",
    }
  ],
  callback  : {
    validate  : TOne.Settings.validateForm,
    update    : TOne.Settings.updateForm
  }
});

// Form: Category
var formCategory = new TONe.Forms.Object({
  title     : "Category",
  items     : [
    {
      type  : "text",
      name  : "name",
      label : "Name"
    }
  ],
  callback  : {
    validate  : TOne.Categories.valdidateForm,
    create    : TOne.Categories.createCatgegory,
    update    : TOne.Categories.updateForm,
    delete    : TOne.Categories.deleteCategory
  }
};

*/

TOne.Forms.parseConfig = function( argConfig ) {
  
  "use strict";
  
  var config = ( "object" === typeof argConfig ? argConfig : {} );
  
  return config;
  
};


TOne.Forms.Object = function( argConfig ) {
  
  "use strict";
  
  this.el     = {};
  this.config = TOne.Forms.parseConfig( argConfig );
  
};

/**
 * @todo parseConfig_createTextInput oder .parseConfig.createTextInput ...
 */

TOne.Forms.createFormItem = function( config ) {
  
  "use strict";
  
  var el = {};
  
  el.item = document.createElement("div");

  el.item.setAttribute( "class", "tone-form-item tone-group" );
  el.item.setAttribute( "data-type", config.type );

  return el;
  
};

TOne.Forms.createLabelCell = function( config ) {
  
  "use strict";
  
  var el = {};
  
  el.div = document.createElement("div");
  el.div.setAttribute( "class", "tone-form-item-label " + config.label.position );
  
  el.label = document.createElement("label");
  el.label.setAttribute( "for", config.label.id );
  el.label.innerText = config.label.text;
  
  el.div.appendChild( el.label );
  
  return el;
  
};

TOne.Forms.createInputCell = function( config ) {
  
  "use strict";
  
  var el = {};
  
  el.div = document.createElement("div");
  el.div.setAttribute( "class", "tone-form-item-input " + config.input.position );

  return el;
  
};

TOne.Forms.createTextInputCell = function( config ) {
  
  "use strict";
  
  var inputCell = TOne.Forms.createInputCell( config );
  
  var input     = document.createElement("input");
  
  input.setAttribute( "type", "text" );
  input.setAttribute( "id", config.input.id );
  input.setAttribute( "value", config.input.value );
  
  inputCell.appendChild( input );
  
  return inputCell;
  
};

TOne.Forms.createItem_textInput = function( config ) {
  
  "use strict";

  var el = {};
  
  // @todo parse config
  config.label.position = "left";
  config.input.position = "right";
  
  
  el.item 	= TOne.Forms.createFormItem( config );
  el.label  = TOne.Forms.createLabelCell( config );
  el.input  = TOne.Forms.createTextInputCell( config );
  
  el.item.appendChild( el.label );
  el.item.appendChild( el.input );
  
  return el;
  
};

