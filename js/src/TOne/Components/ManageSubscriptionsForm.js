import { Subscription }   from "../Data/Subscription.js";
import SubscriptionsAPI   from "../SubscriptionsAPI.js";

TOne.Components.ManageSubscriptionsForm = {
  // data() must be a function - if you want the component to be reusable with SEPARATE data
  data() {

    let _formData     = this.$parent.$data.overlayForm;

    let _formtitle    = _formData.title;

    let _blogName     = _formData.data.subscription.name;
    let _blogTitle    = _formData.data.subscription.title;

    let _categories   = this.$parent.sortedCategoriesByName;

    // Selected category either via Subscription or "Unsorted"
    let _selectedCategoryId = 0;

    if( _formData.data.categoryId > -1 ) {

      _selectedCategoryId = _formData.data.categoryId;

    }
    else {
    
        let _catUnsorted    = this.getCategoryByName(  _categories, "unsorted" );

        _selectedCategoryId = _catUnsorted.id;
  
    }

    // @todo make other services available, too?
    let _services     = [
      {
        name : "Tumblr"
      }
    ];

    let tmpSubscription = null;

    // If a "working" (existing) subscription has been passed/set, use it.
    if(  SubscriptionsAPI.isSubscription( _formData.subscription ) ) {
    
      tmpSubscription = _formData.subscription;
    
    }
    // If not, create a simplistic TEMP subscription object.
    // ... to be returend to where it will be handled, checked, etc...
    else {

      tmpSubscription = new Subscription({
        name    :_blogName,
        title   :_blogTitle
      });

      if( false === tmpSubscription ) {
        // @todo handle error
        console.log( "ERROR");
      }

    }

    console.log( tmpSubscription );

    return  {
      subscriptionForm : {
        formTitle       : _formtitle,
        blogName        : _blogName,
        blogTitle       : _blogTitle
      },
      action            : _formData.action,
      categories        : _categories,
      selectedCategory  : _selectedCategoryId,
      selectedService   : _services[0].name,  // @todo hardcoded, really?!
      subscription      : tmpSubscription
    };
    
  },

  methods : {

    getCategoryByName( categories, name ) {

      let _name     = name.trim().toLowerCase();

      let catIndex  = 0;
      let dimCats   = categories.length;

      for( catIndex = 0; catIndex < dimCats; catIndex += 1 ) {

        let catName = categories[ catIndex ].name.trim().toLowerCase();

        if( catName === _name ) {
          return categories[ catIndex ];
        }

      }

      return false;

    },
    
    validateForm( event ) {

      console.log( "Validate Form: Simple" );
      console.log( this.subscriptionForm );
      console.log( this.selectedCategory );

      let saveData = {
        name        : this.subscriptionForm.blogName,
        title       : this.subscriptionForm.blogTitle,
        service     : this.selectedService,
        categoryId  : this.selectedCategory
      };

      switch( this.action ) {

        case "update":
          this.$root.saveForm_updateSubscription( saveData );
          break;
        
        case "subscribe":
          this.$root.saveForm_addSubscription( saveData );
          break;

      }

      // this.$root.saveForm_addSubscription( saveData );

    },


    closeForm() {
      this.$parent.overlayForm_close();
    }

  },
  template : `
        <form action="javascript:void(0);" method="POST" v-on:submit="validateForm">
          <h2>{{ subscriptionForm.formTitle }} <span v-on:click="closeForm">&#10005;</span></h2>
          <div class="tone-overlayForm-items">
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Blog Name:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <input type="text" readonly v-model="subscriptionForm.blogName" name="subscriptionForm.blogName" id="subscriptionForm.blogName" />
              </div>
            </div>
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Link Label:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <input type="text" v-model="subscriptionForm.blogTitle" name="subscriptionForm.blogTitle" id="subscriptionForm.blogTitle" />
              </div>
            </div>
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Service:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <select size="1" v-model="selectedService">
                  <option value="Tumblr">Tumblr</option>
                </select>
              </div>
            </div>
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Category:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <select size="1" v-model="selectedCategory">
                  <option v-for="( category ) in categories" v-bind:value="category.id" v-html="category.name"></option>
                </select>
              </div>
            </div>
            <div class="tone-overlayForm-item tone-overlayForm-center">
              <button class="tone-overlayForm-btn tone-overlayForm-btnCancel" v-on:click="closeForm">Cancel</button>
              <input type="submit" class="tone-overlayForm-btn tone-overlayForm-btnSubmit" value="Add Or Update Subscription" />
            </div>
          </div>
        </form>
  `
};