
// https://stackoverflow.com/questions/43542418/binding-vue-component-data-to-external-state

TOne.Components.ServiceNavForm = {
  data() {
    return  {
      blogname : ""
    };
  },
  props   : [
    "blgnm"
  ],
  methods : {
    
    getFormData : function() {
      
      /*
       * @todo this feels a bit shady?!
       * But it works in so far as it allows to prioritize the binding
       * to the outside data via prop
       * and still lets you enter text, e.g. for searching
       * 
       * As for subscibing or GOING to "search" terms,
       * let the APIs return errors ... :)
       */
      let elBlogname  = window.document.getElementById( "TOne-ServiceNav-Blog" );
      let elService   = window.document.getElementById( "Tone-ServiceNav-Service" );
      
      let formData    = {
        blogname  : ( null === elBlogname ? ""  : elBlogname.value.trim() ),
        service   : ( null === elService  ? ""  : elService.value.trim() )
      };

      return formData;
      
    },
    
    action_go : function() {
      this.$parent.serviceNav_go( this.getFormData() );
    },

    action_search : function() {
      this.$parent.serviceNav_search( this.getFormData() );
    },

    action_subscribe : function() {
      this.$parent.serviceNav_subscribe( this.getFormData() );
    },

    action_unsubscribe : function() {
      this.$parent.serviceNav_unsubscribe( this.getFormData() );
    },
    
    setBlogname : function( blogname ) {
      this.blogname = blogname.trim();
    }

  },
  template : `
        <form id="TOne-ServiceNav" action="javascript:void(0);" method="POST">
          https://
          <input type="text" v-bind:value="blgnm" id="TOne-ServiceNav-Blog" />
          <select size="1" id="Tone-ServiceNav-Service">
            <option value="Tumblr">.tumblr.com</option>
          </select>
          <button v-on:click="action_go">Go</button>
          <button v-on:click="action_search">Search</button>
          <button v-on:click="action_subscribe">Subscribe</button>
          <button v-on:click="action_unsubscribe">Unsubscribe</button>
        </form>
  `
};