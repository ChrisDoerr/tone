
TOne.Subscription = {};

// @todo Better: TOne.Subscription.NewInstance()
TOne.Subscription.Object = function( argConfig ) {
  
  "use strict";
  
  var config            = ( "object" === typeof argConfig ? argConfig : {} );
  
  var _this             = this;
  

  this.category         = ( "number" === typeof config.category || "string" === typeof config.category ) ? Math.abs( parseInt( config.category, 10 ) ) : 0;
  
  this.service          = ( "string" === typeof config.service ) ? config.service.toLowerCase() : "";
  this.serviceModule    = TOne.App.getServiceModule( this.service );

  this.name             = ( "string" === typeof config.name ) ? config.name : "";
  
  this.title            = ( "string" === typeof config.title ) ? config.title : "";
  
  // @todo parse timestamp
  this.added            = ( "string" === typeof config.added ) ? config.added : "";

  // @todo parse timestamp
  this.lastUpdated      = ( "string" === typeof config.lastUpdated ) ? config.lastUpdated : "";
  
  this.httpStatus       = ( "number" === typeof config.httpStatus || "string" === typeof config.httpStatus ) ? parseInt( config.httpStatus, 10 ) : 200;

  this.status           = ( "string" === typeof config.status ) ? config.status.trim().toLowerCase() : "";
  
  this.el               = {};
  
  this.loadSubscription = function() {

    TOne.Services[ _this.serviceModule ].loadFeed( _this.name );
    // @todo sloppy dezentral!?
    TOne.el.prevNextTop.pageNr.innerText = 1;
    
  };

  this.generateDOM      = function() {
    
    var el = {};
    
    // @todo das icon vor dem Link via li[data-service="tumblr"] stylen/einbinden/anzeigen
    el.li             = document.createElement( "li" );
    el.li.setAttribute( "data-category", _this.category );
    el.li.setAttribute( "data-service", _this.service );
    el.li.setAttribute( "data-name", _this.name );
    el.li.setAttribute( "class", _this.status );
    
    el.a              = document.createElement( "a" );
    el.a.setAttribute( "href", "javascript:void(0);" );
    el.a.setAttribute( "class", "tone-subscription " );
    el.a.innerHTML    = _this.title;
    el.a.addEventListener( "click", _this.loadSubscription );

    el.li.appendChild( el.a );
    
    _this.el = el;
    
  };
  
  this.generateDOM();
  
};
