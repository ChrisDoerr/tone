
import { getDateTimeString } from "./Helpers.js";
import SubscriptionsAPI from "./SubscriptionsAPI.js";
import { Subscriptions } from "./Data/Subscriptions.js";

export default {

    loadSubscriptionsFile( file ) {
        return new Promise(
            ( resolve, reject ) => {

                let _file = ( "string" === typeof file ? file : "./data/subscriptions.json" );

                if( this.fileExists( _file ) ) {

                    fs.readFile(
                        _file,
                        ( err, data ) => {

                            if( err ) {
                                reject( err );
                            }

                            try {

                                let dataObj         = JSON.parse( data );

                                if( "object" !== typeof dataObj.data && !Array.isArray( data.dataObj.data ) ) {

                                    reject( "Invalid subscriptions data file" );

                                }
                                
                                let subscriptions   = new Subscriptions( dataObj.data );
                                // console.log( subscriptions );

                                resolve( subscriptions );
                            
                            }
                            catch( jsonErr ) {

                                reject( jsonErr );

                            }

                        }
                    );

                }
                else {

                    reject( "Subscriptions data file not found!" );

                }

            }
        );
    },

    fileExists( file ) {

        try {
        
            return fs.existsSync( file );

        }
        catch( err ) {
            
            console.error( err );
            return false;
        
        }

    },

    createBackup( file ) {
        return new Promise( ( resolve, reject ) => {

            if( this.fileExists( file ) ) {
    
                let backupFile = file + "." + getDateTimeString() + ".bak";
    
                fs.copyFile(
                    file,
                    backupFile,
                    function( err ) {
                    
                        if( err ) {
    
                            reject( "Backup COULD NOT be created!" );
    
                        }
                        else {
                        
                            resolve();

                        }
    
                    }
                );
    
            }
 
        });

    },

    saveSubscriptions( data ) {
        return new Promise( ( resolve, reject ) => {

            if( !SubscriptionsAPI.isSubscriptions( data ) ) {
                reject( "Invalid data format" );
            }

            let subFile = "./data/subscriptions.json";

            // @todo param / config
            this.createBackup( subFile ).then(
                () => {

                    console.log( "Backup file has been created" );

                    let dataString = JSON.stringify( data );

                    // @todo @next this.writeFile( "")

                    fs.writeFile( subFile, dataString, function( err ) {

                        if( err ) {

                            reject( err );

                        }
                        else {

                        resolve( "The subscriptions file was saved!");

                        }

                    }); 

                },
                ( reason ) => {
                    reject( reason );
                }
            );

        });        
    }

};
