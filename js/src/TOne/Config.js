
/**
 * @requires Node.js module (and global variable) "fs"
 * @global fs
 */
 export default function( configFile ) {

    let rawData = fs.readFileSync( configFile, "utf8" );

    let data    = JSON.parse( rawData );

    // parse config data
    return {
        tumblrCustomerKey : ( "string" === typeof data.tumblrCustomerKey ? data.tumblrCustomerKey.trim() : "" ),
        init : {
            blogname    : ( "string" === typeof data.init.blogname ? data.init.blogname.trim() : "nice-homes" ),
            service     : ( "string" === typeof data.init.service ? data.init.service.trim() : "Tumblr" )
        }
    };

}
