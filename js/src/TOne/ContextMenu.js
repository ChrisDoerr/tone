/**
 * @todo
 */

TOne.ContextMenu = {
  isActive      : false,
  contextTarget : null,
  menus         : []
};


TOne.ContextMenu.addMenu = function( menuSlug, menu ) {
  
  "use strict";
  
  TOne.ContextMenu.menus.push({
    slug : menuSlug,
    menu : menu
  });
  
};

TOne.ContextMenu.Object = function() {
  
  "use strict";
  
  this.el = document.createElement("ul");
  this.el.setAttribute( "class", "contextMenu" );
  
  this.addMenuItem = function( argConfig ) {
    
    var config    = ( "object" === typeof argConfig ) ? argConfig : {};
    
    var title     = ( "string" === typeof config.title ) ? config.title.trim() : "";
    var label     = ( "string" === typeof config.label ) ? config.label.trim() : "";
    var callback  = ( "function" === typeof config.callback ) ? config.callback : null;
    
    var li        = document.createElement( "li" );
    var a         = document.createElement( "a" );
    
    a.setAttribute( "href", "javascript:void(0)" );
    a.setAttribute( "title", title );
    a.innerText   = label;
    
    if( "function" === typeof callback ) {
      a.addEventListener( "click", callback );
    }
    
    li.appendChild( a );
    
    this.el.appendChild( li );
    
  };

};

TOne.ContextMenu.getMenuBySlug = function( menuSlug ) {
  
  "use strict";

  var i         = 0;
  var dimMenus  = TOne.ContextMenu.menus.length;

  for( i = 0; i < dimMenus; i++ ) {
    
    if( TOne.ContextMenu.menus[i].slug === menuSlug ) {
      return TOne.ContextMenu.menus[i].menu;
    }
    
  }

  return null;

};

TOne.ContextMenu.showMenu = function( menuSlug, posX, posY ) {
  
  "use strict";
  
  var menu            = TOne.ContextMenu.getMenuBySlug( menuSlug );
  
  if( null === menu ) {
    return false;
  }
  
  menu.el.style.left  = posX + "px";
  menu.el.style.top   = posY + "px";
  
  document.body.appendChild( menu.el );
  
};

TOne.ContextMenu.closeAllMenus = function() {
  
  "use strict";
  
  TOne.ContextMenu.menus.forEach( function( menu ) {

    TOne.ContextMenu.closeMenu( menu.slug );
    
  });
  
};

TOne.ContextMenu.closeMenu = function( menuSlug ) {
  
  "use strict";
  
  var menu = TOne.ContextMenu.getMenuBySlug( menuSlug );
  
  if( null === menu ) {
    return false;
  }

  try {

    menu.el.parentNode.removeChild( menu.el );
  
    TOne.ContextMenu.isActive       = false;
    TOne.ContextMenu.contextTarget  = null;

    // app.$data.originalImageUrl      = "";
  
  }
  catch( ex )  {
  }
  
};

window.addEventListener( 'contextmenu', function( event ) {
  
  "use strict";
  
  if( event.target.nodeName === "IMG" ) {
    
    TOne.ContextMenu.isActive       = true;
    TOne.ContextMenu.contextTarget  = event.target;

    TOne.ContextMenu.showMenu( "saveImageAs", event.x, event.y );
    
    event.preventDefault();
    
  }
  
});

// INIT
(function(){
  
  // SAVE IMAGE AS menu
  let saveImagesMenu = new TOne.ContextMenu.Object();

  saveImagesMenu.addMenuItem({
    title     : "Save Image As",
    label     : "Save Image As",
    callback  : function( targetElement ) {

      // console.log( "Menu Item Clicked: Save Image As" );
      
        if( null !== TOne.ContextMenu.contextTarget ) {
          TOne.saveImageAs( TOne.ContextMenu.contextTarget.getAttribute( "src" ), TOne.ContextMenu.closeMenu( "saveImageAs" ) );
        }
        else {
          TOne.ContextMenu.closeMenu( "saveImageAs" );
        }

    }
  });
  
  TOne.ContextMenu.addMenu( "saveImageAs", saveImagesMenu );

})();
