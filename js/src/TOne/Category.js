
TOne.Category = {};

TOne.Category.Object = function( argConfig ) {
  
  "use strict";
  
  var config            = ( "object" === typeof argConfig ? argConfig : {} );
  
  var _this             = this;
  
  this.id               = ( "number" === typeof config.id || "string" === typeof config.id ) ? Math.abs( parseInt( config.id, 10 ) ) : -1;
  
  this.name             = ( "string" === typeof config.name ) ? config.name : "";
  
  this.subscriptions    = ( "number" === typeof config.subscriptions || "string" === typeof config.subscription ) ? Math.abs( parseInt( config.subscriptions, 10 ) ) : 0;
  
  this.order  	        = ( "number" === typeof config.order || "string" === typeof config.order ) ? Math.abs( parseInt( config.order, 10 ) ) : 0;
  
  this.el               = {};
  
  this.setSubscriptions = function( argTotal ) {
    
    _this.subscriptions     = Math.abs( parseInt( argTotal, 10 ) );
    
    _this.el.span.innerText = _this.subscriptions;
    
  };
  
  this.loadSubscriptions = function() {
    
    TOne.el.gallery.innerHTML = "";
    
    TOne.App.updateUISubscriptions( _this.id );
    
  };
  
  this.generateLink     = function() {
    
    var el            = {};
    
    el.li             = document.createElement( "li" );
    el.li.setAttribute( "class", "tone-header-category" );
    el.li.setAttribute( "data-category", _this.id );
    
    el.span           = document.createElement( "span" );
    el.span.innerText = _this.subscriptions;

    el.a              = document.createElement( "a" );
    el.a.setAttribute( "href", "javascript:void(0);" );
    el.a.innerText      = _this.name;
    el.a.addEventListener( "click", _this.loadSubscriptions );
    
    el.before         = document.createTextNode( " (" );
    el.after          = document.createTextNode( ")" );
    
    el.li.appendChild( el.a );
    el.li.appendChild( el.before );
    el.li.appendChild( el.span );
    el.li.appendChild( el.after );
    
    _this.el = el;
    
  };
  
  this.generateLink();
  
};
