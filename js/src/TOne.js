
/* Node */
const fs    = require("fs");
const https = require("https");


/* App */
var TOne =  {};


TOne.ucfirst = function( string ) {
  
  "use strict";
  
  return string.charAt(0).toUpperCase() + string.slice(1);

};


TOne.saveImageAs = function( url, callback ) {
  
  "use strict";
  
  var filename  = url.split("/").pop();
  
  var a         = document.createElement("a");

  a.setAttribute( "href", url );
  a.setAttribute( "download", filename );

  a.click();
  
  if( "function" === typeof callback ) {
    callback();
  }
  
};
