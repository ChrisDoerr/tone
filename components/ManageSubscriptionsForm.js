
export default {
  // data() must be a function - if you want the component to be reusable with SEPARATE data
  data() {
  
    let _name = this.$parent.$data.overlayForm.name;
  
    return  {
      subscriptionForm : {
        title : "Foo",
        name : ( "string" === typeof _name ? _name.trim() : "" )
      }
    }
  },
  methods : {

    validateForm( event ) {

      console.log( "Validate Form: Simple" );
      console.log( this.subscriptionForm.name );

      this.$root.saveFormData({
        name  : this.subscriptionForm.name
      });

    },


    closeForm() {
      this.$parent.overlayForm_close();
    }

  },
  template : `
        <form action="javascript:void(0);" method="POST">
          <h2>{{ subscriptionForm.title }} <span v-on:click="closeForm">&#10005;</span></h2>
          <div class="tone-overlayForm-items">
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Blog Name:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <input type="text" />
              </div>
            </div>
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Link Label:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <input type="text" />
              </div>
            </div>
            <div class="tone-overlayForm-item tone-group">
              <div class="tone-overlayForm-item-columnA">
                <label>Service:</label>
              </div>
              <div class="tone-overlayForm-item-columnB">
                <select size="1">
                  <option value="Tumblr">Tumblr</option>
                </select>
              </div>
            </div>
            <div class="tone-overlayForm-item tone-overlayForm-center">
              <button class="tone-overlayForm-btn tone-overlayForm-btnCancel" v-on:click="closeForm">Cancel</button>
              <input type="submit" class="tone-overlayForm-btn tone-overlayForm-btnSubmit" value="Add Subscription" />
            </div>
          </div>
        </form>
  `
};